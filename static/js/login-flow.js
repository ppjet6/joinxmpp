/*
 * login-flow.js
 * Copyright (C) 2019 Maxime “pep” Buquet <pep@bouah.net>
 *
 * Distributed under terms of the AGPLv3+ license.
 */
(function(document){
  'use strict';

  const XHTML_NS = 'http://www.w3.org/1999/xhtml';

  const SERVER_ID = 'server';
  const JID_ID = 'jid';
  const AVAILABILITY_ID = 'availability';
  const APP_CHOOSER_ID = 'app-chooser';

  let SELECTED_SERVER = undefined;

  let nodeprep = function (input) {
    return input;
  };

  let find_server = function () {
    SELECTED_SERVER = 'foo.bar';
  };

  let display_server = function (server) {
  };

  let display_jid = function (username, server) {
    let jid = document.getElementById(JID_ID);

    if (username == '' || SELECTED_SERVER === undefined) {
      jid.value = '';
      return;
    }

    jid.value = username + '@' + server;
  };

  let fill_jid = function (event) {
    event.preventDefault();
    // TODO: Validate username
    let input = event.target.value;
    // let username = nodeprep(input);
    let username = nodeprep(input);
    // TODO: Display JID (username + server)
    display_jid(username, SELECTED_SERVER);
  };

  let main = function () {
    find_server(); 

    let user_field = document.getElementById('username');
    user_field.addEventListener('input', fill_jid);
  };

  main();
})(document);
