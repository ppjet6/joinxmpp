let map;

function initmap() {
	// set up the map
	map = new L.Map('map');

	// create the tile layer with correct attribution
	var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});

	// start the map in South-East England
	map.setView([45.7679, 4.8506], 6);
	map.addLayer(osm);

	const marker = new L.Marker([45.7679, 4.8506]).addTo(map);
	console.log(marker)

	const xhr = new XMLHttpRequest();
	xhr.addEventListener('load', displayMarkers);
	xhr.responseType = 'json';
	// TODO: maybe get the markers only in the current bounds?
	xhr.open('GET', '../markers.json');
	xhr.send();
}

function displayMarkers() {
	const servers = this.response;
	for (let server of servers) {
		if (server.location === null) {
			console.log('Server ' + server.domain + ' has no location, skipping.');
			continue;
		}
		const options = {};
		if (server.logo !== null) {
			options.icon = L.icon({
				iconUrl: server.logo,
				iconSize: [32, 32],
				iconAnchor: [16, 16],
				popupAnchor: [0, -16],
			});
		}
		const marker = new L.Marker(server.location, options);
		marker.data = server;
		marker.addTo(map);
		marker.bindPopup("<h3>" + server.domain + "</h3><p>" + server.uptime + "% uptime</p>");
	}
}
