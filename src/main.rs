#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use std::path::Path;
use std::net::IpAddr;
use geoip::{self, GeoIp};
use geoutils::Location;

use rocket::Outcome;
use rocket::request::{self, Request, FromRequest};
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use tera::Context;

#[derive(Debug)]
struct XForwardedFor(pub Option<IpAddr>);

impl<'a, 'r> FromRequest<'a, 'r> for XForwardedFor {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let mut ips: Vec<_> = request.headers().get("X-Forwarded-For").collect();
        if let Some(ip) = ips.pop() {
            if let Ok(ip) = ip.parse() {
                Outcome::Success(XForwardedFor(Some(ip)))
            } else {
                Outcome::Success(XForwardedFor(None))
            }
        } else {
            Outcome::Success(XForwardedFor(None))
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Server {
    domain: String,
    logo: Option<String>,
    country: String,
    uptime: f32,
    compliance: u8,
    tls: String,
    privacy_policy: Option<String>,
    fee: String,
    location: Option<(f32, f32)>,
}

impl Server {
    fn new<'a, 'b, L: Into<Option<&'a str>>, P: Into<Option<&'b str>>>(domain: &str, logo: L, country: &str, uptime: f32, compliance: u8, tls: &str, privacy_policy: P, fee: &str) -> Server {
        Server {
            domain: String::from(domain),
            logo: logo.into().map(Into::into),
            country: String::from(country),
            uptime,
            compliance,
            tls: String::from(tls),
            privacy_policy: privacy_policy.into().map(Into::into),
            fee: String::from(fee),
            location: None,
        }
    }
}

lazy_static! {
    static ref SERVERS: Vec<Server> = {
        let mut servers = [
            Server::new("blabber.im", None, "Germany", 100., 19, "A", "https://blabber.im/en/datenschutz/", "free"),
            Server::new("conversations.im", None, "Germany", 99.52, 19, "A", "https://account.conversations.im/privacy/", "8€/mo"),
            Server::new("jabber.fr", "https://chat.jabberfr.org/avatar/jabberfr@chat.jabberfr.org", "France", 99.72, 19, "A", "https://wiki.jabberfr.org/CGU", "free"),
            Server::new("xmpp.jp", None, "Japan", 100., 19, "A", None, "free"),
        ];

        let geoip = match GeoIp::open(Path::new("/usr/share/GeoIP/GeoIPCity.dat"), geoip::Options::Standard) {
            Ok(geoip) => geoip,
            Err(_) => return vec![],
        };

        for server in servers.iter_mut() {
            if let Some(info) = geoip.city_info_by_name(&server.domain) {
                let location = (info.latitude, info.longitude);
                server.location = Some(location);
            }
        }

        servers.iter().cloned().collect()
    };
}

#[get("/")]
fn index() -> Template {
    let context = Context::new();
    Template::render("layout", context)
}

#[get("/pages")]
fn apis() -> rocket::response::content::Html<&'static str> {
    rocket::response::content::Html("<ul>
    <li><a href='markers.json'>/markers.json</a></li>
    <li><a href='ip'>/ip</a></li>
    <li><a href='location'>/location</a></li>
    <li><a href='distance/blabber.im'>/distance/blabber.im</a></li>
    <li><a href='distance/conversations.im'>/distance/conversations.im</a></li>
    <li><a href='distance/jabber.fr'>/distance/jabber.fr</a></li>
    <li><a href='distance/xmpp.jp'>/distance/xmpp.jp</a></li>
    <li><a href='distance/invalid.example'>/distance/invalid.example</a></li>
    <li><a href='map/index.xhtml'>/map/index.xhtml</a></li>
</ul>")
}

#[get("/markers.json", format = "application/json")]
fn markers() -> Json<Vec<Server>> {
    Json(SERVERS.iter().cloned().collect())
}

#[get("/ip")]
fn ip(addr: std::net::SocketAddr, x_forwarded_for: XForwardedFor) -> String {
    let ip = match x_forwarded_for.0 {
        Some(ip) => ip,
        None => addr.ip(),
    };
    format!("{}", ip)
}

#[get("/location")]
fn location(addr: std::net::SocketAddr, x_forwarded_for: XForwardedFor) -> String {
    let ip = match x_forwarded_for.0 {
        Some(ip) => ip,
        None => addr.ip(),
    };

    // TODO: don’t open it on every request.
    let geoip = match GeoIp::open(Path::new("/usr/share/GeoIP/GeoIPCityv6.dat"), geoip::Options::Standard) {
        Ok(geoip) => geoip,
        Err(_) => return String::from("location error"),
    };

    match geoip.city_info_by_ip(ip) {
        Some(info) => format!("{:#?}", info),
        None => String::from("apatride :’("),
    }
}

#[get("/distance/<domain>")]
fn distance(domain: &rocket::http::RawStr, addr: std::net::SocketAddr, x_forwarded_for: XForwardedFor) -> String {
    let domain = domain.as_str();
    let ip = match x_forwarded_for.0 {
        Some(ip) => ip,
        None => addr.ip(),
    };

    // TODO: don’t open it on every request.
    let geoip = match GeoIp::open(Path::new("/usr/share/GeoIP/GeoIPCityv6.dat"), geoip::Options::Standard) {
        Ok(geoip) => geoip,
        Err(_) => return String::from("distance error"),
    };

    match geoip.city_info_by_ip(ip) {
        Some(info) => {
            let location = Location::new(info.latitude, info.longitude);
            let mut distance = std::f64::NAN;
            for server in SERVERS.iter() {
                if server.domain == domain {
                    if let Some(server_location) = server.location {
                        let server_location = Location::new(server_location.0, server_location.1);
                        distance = location.haversine_distance_to(&server_location);
                    }
                }
            }
            format!("{} km", distance / 1000.)
        },
        None => String::from("Far far away…"),
    }
}

fn main() {
    rocket::ignite()
        .attach(Template::fairing())
        .mount("/", routes![index, markers, ip, location, distance])
        .mount("/map", StaticFiles::from("static"))
        .mount("/js", StaticFiles::from("static/js"))
        .mount("/css", StaticFiles::from("static/css"))
        .launch();
}
